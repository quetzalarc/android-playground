package com.quetzalstudio.androidstudioplayground;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    String host;
    String orderAPI;

    Button buttonRequest;
    TextView textResponse;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonRequest = (Button) findViewById(R.id.button_send);
        textResponse = (TextView) findViewById(R.id.text_response);

        buttonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();

                Log.i("APP", "Button request clicked");
            }
        });

        progressDialog = new ProgressDialog(MainActivity.this);
    }

    protected void sendRequest() {
        VolleyLog.DEBUG = true;

        host = getString(R.string.api_host);
        orderAPI = host + getString(R.string.api_post_order);

        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                orderAPI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        textResponse.setText(response);

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body;

                        if (error.networkResponse.data != null) {
                            try {
                                body = new String(error.networkResponse.data, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                body = e.getMessage();
                            }
                        }
                        else {
                            body = error.getMessage();
                        }

                        textResponse.setText(body);

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> formData = new HashMap<String, String>();
                formData.put("apiKey", getString(R.string.api_token));
                formData.put("name", "Cleaning Service");
                formData.put("date", "16-02-2018");
                formData.put("time", "10:10");
                formData.put("promo", "UK50");
                formData.put("note", "From example request :: ata");
                formData.put("address", "Jl. KH. A. Sanusi");
                formData.put("typeGedung", "HOME");
                formData.put("gender", "Man");
                formData.put("pet", "Yes");
                formData.put("amount_bath", "10");
                formData.put("amount_bed", "10");
                formData.put("amount_other", "10");

                return formData;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        requestQueue.add(stringRequest);
    }
}
